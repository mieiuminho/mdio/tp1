# Explicação do modelo {#sec:model}

Devemos começar por referir que o maior número de aluno do nosso grupo de
trabalho é o do aluno Hugo Filipe Duarte Carvalho (A85579).

Assim, de acordo com as indicações presentes no enunciado completamos o nosso
mapa, atribuindo o devido sentido às ruas B, C, D e E.

Consideremos o número antes referido na forma apresentada no enunciado (ABCDE):
podemos estabelecer a seguinte correspondência: A-8, B-5, C-5, D-7 e E-9. Como o
número que corresponde a B é ímpar a rua B é a subir. O número que corresponde a
C é 5 que é ímpar, portanto a rua C tem sentido para a esquerda. O número que
corresponde a D é 7, pelo que a rua é a subir. Por último, o número que
corresponde a E é 9 pelo que a rua E tem sentido para a esquerda.

![Mapa Completo](figures/mapa.png){ height=430px }

A estratégia que utilizamos para modelar o problema foi estabelecer um conjunto
de vértices, que representam os pontos em que pode ser tomada uma decisão em
relação à direção a seguir.

![Mapa com vértices](figures/mapaVertices.png){ height=300px }

Para modelar o problema precisamos de variáveis de decisão, pelo que podemos
usar a seguinte regra de nomenclatura para as mesmas: $x_{ab}$, que simbolizam
arcos, em que $a$ é o vértice em que se inicia o arco e $b$ o vértice onde o
mesmo termina.

No entanto, é importante clarificar o raciocínio que nos levou a determinar os
vértices da forma que apresentamos acima. Um vértice, no contexto do problema,
será um ponto onde o condutor do camião teria a possibilidade de escolher dois
caminhos diferentes, respeitando o sentido das ruas (não conduzindo em
contramão). Desta forma, obtemos apenas os vértices apresentados antes. Vamos
então ver porque podemos excluir os outros vértices:

![Mapa com os vértices excluídos](figures/naoVertices.png){ height=300px }

Nenhum dos pontos representados a vermelho na imagem constituí um vértice no
contexto do problema, uma vez que nenhum deles implica uma decisão, ou seja, só
existe um caminho possível de modo a que o camionista se mantenha no sentido
correto.

Sabemos que do modelo de programação linear têm de constar uma função objetivo e
as condições a que a mesma está sujeita. Assim, perante o contexto do problema
apresentado percebemos que a função objetivo é aquela que espelha a distância
percorrida pelo camionista. De acordo com a regra que estabelecemos para a
nomenclatura das variáveis de decisão temos a seguinte função objetivo:

$min$ $z$ $= 3x_{oa} + 9x_{ac} + 12x_{co} + 7x_{ab} + 4x_{cc} + 5x_{be} 2x_{eb} + 10 x_{de} + 3x_{ef} + 8x_{fb} + 7x_{dg} + 4x_{gf} + 10x_{fc} + 16x_{bg} + 14x_{go}$

Relativamente às condições: sabemos que um dos objetivos do problema é que sejam
visitados todos os troços da rede de caminhos, com efeito, torna-se óbvio que o
valor de todas as variáveis de decisão numa solução admissível para o problema
tem de ser maior ou igual a 1.

$x_{ac} >= 1$; $x_{co} >= 1$; $x_{cc} >= 1$; $x_{ab} >= 1$; $x_{be} >= 1$;
$x_{ed} >= 1$; $x_{de} >= 1$; $x_{ef} >= 1$; $x_{fb} >= 1$; $x_{dg} >= 1$;
$x_{gf} >= 1$; $x_{fc} >= 1$; $x_{bg} >= 1$; $x_{go} >= 1$;

Temos obrigatoriamente de garantir que o circuito é possível, isto é, partir do
depósito e percorrer todas as ruas voltando à posição inicial. Temos também de
garantir que cada vez que visitamos um vértice saímos do mesmo, para isso basta
que forcemos o grau de entrada dos vértices a ser igual ao respetivo grau de
saída. Importa referir que que vértice CC não necessita de condição que pondere
os seus graus de saída e entrada, uma vez que se parte do vértice C para ir ter
ao vértice C. Assim surgem as seguintes condições:

$x_{oa} = x_{go} + x_{co}$; $x_{ac} + x_{ab} = x_{oa}$; $x_{be} + x_{bg} =
x_{ab} + x_{fb}$; $x_{co} = x_{ac} + x_{fc}$; $x_{dg} + x_{de} = x_{ed}$;
$x_{ed} + x_{ef} = x_{be} + x_{de}$; $x_{fb} + x_{fc} = x_{ef} + x_{gf}$;
$x_{gf} + x_{go} = x_{dg} + x_{bg}$

