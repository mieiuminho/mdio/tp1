# Validação da solução {#sec:validation}

Para validarmos a solução encontrada para o problema proposto tomamos como ponto
de partida as imagens correspondentes às iterações do percurso ótimo
apresentadas acima.

Analisando as imagens percebemos que a solução apresentada para o problema
cumpre todas as condicionantes descritas no enunciado: todos os arcos são
visitados, isto é o camião passa por todas as ruas; o percurso principal começa
e termina no depósito;

É importante notarmos que o grau de saída e o grau de entrada em cada vértice
são iguais, conforme tínhamos forçado acima.

* Vértice **O**:
$x_{oa} = 3$ (portanto o grau de saída é 3)
$x_{go} = 1$ e $x_{co} = 2$ (portanto o grau de entrada é 3)

* Vértice **A**:
$x_{ac} = 1$ e $x_{ab} = 2$ (portanto o grau de saída é 3)
$x_{oa} = 3$ (portanto o grau de entrada é 3)

* Vértice **B**:
$x_{be} = 2$ e $x_{bg} = 1$ (portanto o grau de saída é 3)
$x_{ab} = 2$ e $x_{fb} = 1$ (portanto o grau de entrada é 3)

* Vértice **C**:
$x_{co} = 2$ (portanto o grau de saída é 2)
$x_{ac} = 1$ e $x_{fc} = 1$ (portanto o grau de entrada é 2)

* Vértice **D**:
$x_{dg} = 1$ e $x_{de} = 1$ (portanto o grau de saída é 2)
$x_{ed} = 2$ (portanto o grau de entrada é 2)

* Vértice **E**:
$x_{ed} = 2$ e $x_{ef} = 1$ (portanto o grau de saída é 3)
$x_{be} = 2$ e $x_{de} = 1$ (portanto o grau de entrada é 3)

* Vértice **F**:
$x_{fb} = 1$ e $x_{fc} = 1$ (portanto o grau de saída é 2)
$x_{ef} = 1$ e $x_{gf} = 1$ (portanto o grau de entrada é 2)

* Vértice **G**:
$x_{gf} = 1$ e $x_{go} = 1$ (portanto o grau de saída é 2)
$x_{dg} = 1$ e $x_{bg} = 1$ (portanto o grau de entrada é 2)

