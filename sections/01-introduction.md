# Introdução {#sec:intro}

Há situações em que se pretende determinar o circuito, ou conjunto de
circuitos, em que todos os arcos de um grafo são percorridos, pelo menos uma
vez, minimizando a distância total percorrida. Note-se que pode ser necessário
atravessar o mesmo arco mais do que uma vez, para que sejam alcançados tais
objetivos.

No caso vertente, temos o mapa de uma zona de uma cidade e sabemos que um
camião de recolha de lixo parte de um ponto inicial e tem de passar por todas
as ruas representadas nesse mapa e voltar ao ponto inicial para deixar o lixo
no depósito, minimizando a distância percorrida. É fulcral que o veículo parta
do depósito e acabe o percurso no depósito para que possa sempre depositar o
lixo recolhido no mesmo. É ainda de notar que as ruas são de sentido único.

