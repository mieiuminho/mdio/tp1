OPTIONS = --template=styles/template.tex --filter pandoc-crossref --filter pandoc-include-code
CONFIG  = --metadata-file config.yml
BIB     = --filter pandoc-citeproc --bibliography=references.bib
SRC     = $(shell ls $(SRC_DIR)/*.md)
SRC_DIR = sections
PROJECT = report

$(PROJECT):
	pandoc $(CONFIG) $(OPTIONS) $(BIB) -s $(SRC) -o $(PROJECT).pdf

run:
	@lp_solve solution/solution.lp

clean:
	@echo "Cleaning..."
	@-cat .art/maid.ascii
	@rm $(PROJECT).pdf
	@echo "...✓ done!"
