# Solução Ótima {#sec:solution}

Nesta secção apresentaremos uma solução para o problema proposto. Importa
referir que esta solução não é única, podiam ser escolhidas maneiras diferentes
de realizar o percurso. Por forma a facilitar a apresentação da solução iremos
dividi-la em iterações. Vamos considerar uma iteração como sendo um sub-percurso
com início e fim no depósito de lixo.

Esta é apenas uma rota possível uma vez que poderíamos arranjar outras soluções
para o problema: basta pensarmos que podemos rearranjar a ordem em que acontecem
as iterações consideradas e podem ainda ser criadas iterações com diferentes
arranjos de arcos (pequenas variantes da solução que aqui apresentamos, sendo
que o valor da função objetivo não se altera).

Ordem pela qual são visitados os arcos na iteração 1:

$$OA \rightarrow AB \rightarrow BE \rightarrow ED \rightarrow DE \rightarrow ED
     \rightarrow DG \rightarrow GF \rightarrow FC \rightarrow CC
     \rightarrow CO$$

![Iteração nº 1](figures/iteracao1.png){ height=400px }

\newpage

Ordem pela qual são visitados os arcos na iteração 2:

$$OA \rightarrow AC \rightarrow CO$$

![Iteração nº 2](figures/iteracao2.png){ height=300px }

Ordem pela qual são visitados os arcos na iteração 3:

$$OA \rightarrow AB \rightarrow BE \rightarrow EF \rightarrow FB \rightarrow BG
     \rightarrow GO$$

![Iteração nº 3](figures/iteracao3.png){ height=300px }

