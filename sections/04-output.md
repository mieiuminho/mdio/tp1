# Output {#sec:output}

```
Value of objective function: 146.00000000

Actual values of the variables:

Xoa                  3
Xac                  1
Xco                  2
Xcc                  1
Xab                  2
Xbe                  2
Xed                  2
Xde                  1
Xef                  1
Xfb                  1
Xdg                  1
Xgf                  1
Xfc                  1
Xbg                  1
Xgo                  1
```

