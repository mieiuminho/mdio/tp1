# Ficheiro de Input {#sec:input}

A função objetivo é definida como:

```{.lp include=solution/solution.lp startLine=2 endLine=4}
```

\newpage

As restrições são apresentadas de seguida:

```{.lp include=solution/solution.lp startLine=8 endLine=32}
```

